﻿using System;
using System.Collections.Generic;


namespace Main
{
    class Program 
    {
        static void Main()
        {
            List<Action> act = new List<Action>()
            {new Addition(5), 
                new Multiplication(3),
                new Subtraction(6)};
            Сalculator cal = new Сalculator(1, act);
            
            
            Console.WriteLine(cal.FindResult());
        }
    }
    
    public class Сalculator
    {
        List<Action> Actions;
        private double _result;
        
        public Сalculator(double startResult, List<Action> actions)
        {
            Actions = actions;
            _result = startResult;
        }
        public double FindResult()
        {
            foreach (Action a in Actions)
            {
                _result = a.Calculate(_result);
            }

            return _result;
        }
    }

    public abstract class Action
    {
        protected double B;
        protected Action(double b)
        {
            B = b;
        }
        public abstract double Calculate(double a);
    }

    public class Addition : Action
    {
        public Addition(double b) : base(b) {}
        public override double Calculate(double a) => a + B;

    }

    public class Subtraction : Action
    {
        public Subtraction(double b) : base(b) {}
        public override double Calculate(double a) => a - B;
    }

    public class Multiplication : Action
    {
        public Multiplication(double b) : base(b) {}
        public override double Calculate(double a) => a * B;
    }

    public class Division : Action
    {
        public Division(double b) : base(b) {}
        public override double Calculate(double a) => a / B;
    }
}


